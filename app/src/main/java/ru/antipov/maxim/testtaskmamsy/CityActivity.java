package ru.antipov.maxim.testtaskmamsy;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.BindView;
import ru.antipov.maxim.testtaskmamsy.Fragment.CityFragment;
import ru.antipov.maxim.testtaskmamsy.Utils.Utils;

public class CityActivity extends AppCompatActivity {
    private String[] arr;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    FragmentStatePagerAdapter mFragmentStatePagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        if(Utils.getArrayCity(getApplicationContext()) == null) {
            arr = new String[] {"Moscow", "London", "Minsk", "Washington", "Madrid", "Rome"};
            Utils.setArrayCity(getApplicationContext(), arr);
        } else {
            arr = Utils.getArrayCity(getApplicationContext());
        }

        FragmentManager fragmentManager = getSupportFragmentManager();

        mFragmentStatePagerAdapter = new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int i) {
                Fragment fragment = CityFragment.newInstance(arr[i]);

                ((CityFragment) fragment).setNotifyViewPager(new CityFragment.notifyViewPager() {
                    @Override
                    public void notifyPager() {
                        arr = Utils.getArrayCity(getApplicationContext());
                        mFragmentStatePagerAdapter.notifyDataSetChanged();
                    }
                });

                return fragment;
            }

            @Override
            public int getCount() {
                return arr.length;
            }
        };

        mViewPager = (ViewPager) findViewById(R.id.view_pager);

        mViewPager.setAdapter(mFragmentStatePagerAdapter);
    }
}
