package ru.antipov.maxim.testtaskmamsy.JSONClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coords {
    @SerializedName("lon")
    @Expose
    private double lon;

    @SerializedName("lat")
    @Expose
    private double lat;

    public String getLon() {
        return Double.toString(lon);
    }

    public String getLat() {
        return Double.toString(lat);
    }
}
