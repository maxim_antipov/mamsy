package ru.antipov.maxim.testtaskmamsy.JSONClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WeatherData {
    @SerializedName("city")
    @Expose
    private City city;

    @SerializedName("list")
    @Expose
    private ArrayList<DList> list;

    public City getCity() {
        return city;
    }

    public List<DList> getList() {
        return list;
    }
}
