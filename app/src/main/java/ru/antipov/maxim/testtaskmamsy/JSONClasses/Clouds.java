package ru.antipov.maxim.testtaskmamsy.JSONClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Clouds {
    @SerializedName("all")
    @Expose
    private int all;

    public String getAll() {
        return Integer.toString(all);
    }
}
