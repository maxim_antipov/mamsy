package ru.antipov.maxim.testtaskmamsy;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class SingletonNetwork {
    private static Api singleton;

    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    private static void create() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        singleton = retrofit.create(Api.class);
    }

    public static Api getSingleton() {
        if(singleton == null) {
            create();
        }

        return singleton;
    }
}
