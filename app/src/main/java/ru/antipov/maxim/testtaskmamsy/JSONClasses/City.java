package ru.antipov.maxim.testtaskmamsy.JSONClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class City {
    @SerializedName("coord")
    @Expose
    private Coords coords;

    @SerializedName("country")
    @Expose
    private String country;

    public Coords getCoords() {
        return coords;
    }

    public String getCountry() {
        return country;
    }
}
