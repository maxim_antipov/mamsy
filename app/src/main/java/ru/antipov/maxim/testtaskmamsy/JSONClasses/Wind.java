package ru.antipov.maxim.testtaskmamsy.JSONClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wind {
    @SerializedName("speed")
    @Expose
    private double speed;

    public String getSpeed() {
        return Double.toString(speed);
    }
}
