package ru.antipov.maxim.testtaskmamsy.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.antipov.maxim.testtaskmamsy.DialogFragment.AddCityDialogFragment;
import ru.antipov.maxim.testtaskmamsy.JSONClasses.WeatherData;
import ru.antipov.maxim.testtaskmamsy.R;
import ru.antipov.maxim.testtaskmamsy.Utils.Utils;

public class CityFragment extends Fragment implements MView {
    private static final String ARG_NAME_CITY = "name_city";

    @BindView(R.id.tvCity)
    TextView tvCity;

    @BindView(R.id.llDataWeather)
    LinearLayout llDataWeather;

    @BindView(R.id.loadingMain)
    View loading;

    private Presenter presenter;
    private String name_city;

    public interface notifyViewPager {
        void notifyPager();
    }

    notifyViewPager mNotifyViewPager;

    public static CityFragment newInstance(String city_name) {
        Bundle args = new Bundle();
        args.putString(ARG_NAME_CITY, city_name);

        CityFragment fragment = new CityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setNotifyViewPager(notifyViewPager notifyViewPager) {
        mNotifyViewPager = notifyViewPager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        name_city = (String) getArguments().get(ARG_NAME_CITY);

        presenter = new Presenter(new Model());
        presenter.attachView(this);

        presenter.setCityName(getContext(), name_city);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_city, container, false);
        ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_city_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_city:
                AddCityDialogFragment addCityDialogFragment = AddCityDialogFragment.newInstance();

                addCityDialogFragment.setAddCityCallback(new AddCityDialogFragment.AddCityCallback() {
                    @Override
                    public void addCity() {
                        mNotifyViewPager.notifyPager();
                    }
                });

                addCityDialogFragment.show(getActivity().getSupportFragmentManager(), "dialog");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void putDataWeather(WeatherData weatherData) {
        putData(tvCity, getString(R.string.city_country, name_city, weatherData.getCity().getCountry()));

        for(int i = 0; i < Utils.DAYS; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_WEEK, i);

            String day = getString(R.string.day, getTextDay(calendar));

            TextView textViewDay = new TextView(getContext());
            textViewDay.setTextSize(20.0f);
            textViewDay.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            putData(textViewDay, day);
            llDataWeather.addView(textViewDay);

            for(int j = 8 * i; j < 8 + 8 * i; j++) {
                try {
                    Date date = new java.util.Date(weatherData.getList().get(j).getDt() * 1000L);
                    SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss z");
                    String weather = getString(R.string.template_temperature,
                            sdf.format(date),
                            weatherData.getList().get(j).getWeather().get(0).getDescription(),
                            weatherData.getList().get(j).getMain().getTemp(),
                            weatherData.getList().get(j).getMain().getTempMin(),
                            weatherData.getList().get(j).getMain().getTempMax(),
                            weatherData.getList().get(j).getWind().getSpeed(),
                            weatherData.getList().get(j).getClouds().getAll(),
                            weatherData.getList().get(j).getMain().getPressure());

                    TextView textViewWeather = new TextView(getContext());
                    textViewWeather.setTextSize(16.0f);
                    textViewWeather.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    putData(textViewWeather, weather);
                    llDataWeather.addView(textViewWeather);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        String coords = getString(R.string.coords,
                weatherData.getCity().getCoords().getLat(),
                weatherData.getCity().getCoords().getLon());

        TextView textViewCoords = new TextView(getContext());
        textViewCoords.setTextSize(16.0f);
        textViewCoords.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));
        putData(textViewCoords, coords);

        llDataWeather.setVisibility(View.VISIBLE);
        loading.setVisibility(View.GONE);
    }

    @Override
    public void textMessage(String message) {
        if(getActivity() != null) {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    private void putData(TextView textView, String data) {
        textView.setText(Html.fromHtml(data));
    }

    private String getTextDay(Calendar calendar) {
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                return "Monday";
            case Calendar.TUESDAY:
                return "Tuesday";
            case Calendar.WEDNESDAY:
                return "Wednesday";
            case Calendar.THURSDAY:
                return "Thursday";
            case Calendar.FRIDAY:
                return "Friday";
            case Calendar.SATURDAY:
                return "Saturday";
            case Calendar.SUNDAY:
                return "Sunday";
            default:
                return null;
        }
    }
}
