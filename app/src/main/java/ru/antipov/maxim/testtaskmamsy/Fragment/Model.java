package ru.antipov.maxim.testtaskmamsy.Fragment;

import android.util.Log;

import ru.antipov.maxim.testtaskmamsy.JSONClasses.WeatherData;
import ru.antipov.maxim.testtaskmamsy.SingletonNetwork;
import ru.antipov.maxim.testtaskmamsy.Utils.Utils;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Model {
    public interface LoadCallback {
        void onLoad(WeatherData weatherData);
        void onError(String error);
    }

    public void getWeatherFor5Days(String city, final LoadCallback loadCallback) {
        Observable<WeatherData> observable = SingletonNetwork.getSingleton().get5DaysData(city, String.valueOf(Utils.Units.Metric), Utils.API_KEY);
        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<WeatherData>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadCallback.onError(e.toString());
                        e.getStackTrace();
                    }

                    @Override
                    public void onNext(WeatherData weatherData) {
                        loadCallback.onLoad(weatherData);
                    }
                });
    }
}
