package ru.antipov.maxim.testtaskmamsy;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.antipov.maxim.testtaskmamsy.JSONClasses.WeatherData;
import rx.Observable;

public interface Api {
    @GET("forecast")
    Observable<WeatherData> get5DaysData(@Query("q")     String city_name,
                                         @Query("units") String units,
                                         @Query("APPID") String key);
}
