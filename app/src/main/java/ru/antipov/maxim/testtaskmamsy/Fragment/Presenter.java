package ru.antipov.maxim.testtaskmamsy.Fragment;

import android.content.Context;
import android.util.Log;

import ru.antipov.maxim.testtaskmamsy.JSONClasses.WeatherData;
import ru.antipov.maxim.testtaskmamsy.Utils.Utils;

public class Presenter {
    private Model model;
    private MView view;

    public Presenter(Model model) {this.model = model;}
    public void attachView(MView view) {this.view = view;}

    public void setCityName(Context context, String city_name) {
        if(!Utils.hasConnection(context)) {
            view.textMessage("Not internet!");
            return;
        }

        model.getWeatherFor5Days(city_name, new Model.LoadCallback() {
            @Override
            public void onLoad(WeatherData weatherData) {
                view.putDataWeather(weatherData);
            }

            @Override
            public void onError(String error) {
                view.textMessage(error);
            }
        });
    }
}
