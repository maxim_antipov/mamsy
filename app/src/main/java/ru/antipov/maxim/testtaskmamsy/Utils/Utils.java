package ru.antipov.maxim.testtaskmamsy.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.util.ArraySet;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class Utils {
    public  static final String API_KEY = "8b2794408a314e9f6494ff5e2e2efb5b";
    private static final String ARRAY = "ARRAY";

    public enum Units {
        Imperial("imperial"),
        Metric("metric");

        private final String name;

        Units(String s) {
            name = s;
        }

        public String toString() {
            return this.name;
        }
    }

    public static final int DAYS = 5;

    public static String[] getArrayCity(Context context) {
        SharedPreferences sPref = context.getSharedPreferences("Utils", Context.MODE_PRIVATE);

        ArrayList<String> arrayList = new ArrayList();

        JSONArray jsonArray = null;

        try {
            jsonArray = new JSONArray(sPref.getString(ARRAY, "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                arrayList.add((String) jsonArray.get(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(arrayList.size() == 0)
            return null;

        return arrayList.toArray(new String[arrayList.size()]);
    }

    public static void setArrayCity(Context context, String[] arr) {
        SharedPreferences sPref = context.getSharedPreferences("Utils", Context.MODE_PRIVATE);

        ArrayList<String> array = new ArrayList<>();

        for(int i = 0; i < arr.length; i++)
            array.add(arr[i]);

        JSONArray jsonArray = new JSONArray(array);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString(ARRAY, jsonArray.toString());
        editor.commit();
    }

    public static boolean hasConnection(final Context context) {
        if(context == null) {
            return false;
        }

        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
