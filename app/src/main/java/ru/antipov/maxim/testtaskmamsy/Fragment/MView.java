package ru.antipov.maxim.testtaskmamsy.Fragment;

import ru.antipov.maxim.testtaskmamsy.JSONClasses.WeatherData;

public interface MView {
    void putDataWeather(WeatherData weatherData);
    void textMessage(String message);
}
