package ru.antipov.maxim.testtaskmamsy.DialogFragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.antipov.maxim.testtaskmamsy.R;
import ru.antipov.maxim.testtaskmamsy.Utils.Utils;

public class AddCityDialogFragment extends DialogFragment {
    public static AddCityDialogFragment newInstance() {
        AddCityDialogFragment fragment = new AddCityDialogFragment();
        return fragment;
    }

    public interface AddCityCallback {
        void addCity();
    }

    AddCityCallback mAddCityCallback;

    @BindView(R.id.etCity) EditText etCity;
    @BindView(R.id.btnAddCity) Button btnAddCity;

    public void setAddCityCallback(AddCityCallback addCityCallback) {
        mAddCityCallback = addCityCallback;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.add_city_dialog_fragment, null);
        ButterKnife.bind(this, view);

        btnAddCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etCity.getText().toString().equals("")) {
                    dismiss();
                } else {
                    boolean isHas = false;

                    String[] arr = Utils.getArrayCity(getContext());
                    ArrayList<String> arrL = new ArrayList<>();

                    for(int i = 0; i < arr.length; i++) {
                        if(arr[i].equals(etCity.getText().toString())) {
                            isHas = true;
                            break;
                        }
                            arrL.add(arr[i]);
                    }

                    if(isHas) {
                        dismiss();
                        return;
                    }

                    arrL.add(etCity.getText().toString());
                    Utils.setArrayCity(getContext(), arrL.toArray(new String[arrL.size()]));

                    mAddCityCallback.addCity();

                    dismiss();
                }
            }
        });

        return builder.setView(view)
                .create();
    }

    @Override
    public void show(FragmentManager manager, String tag) {

        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }
}
