package ru.antipov.maxim.testtaskmamsy.JSONClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Main {
    @SerializedName("temp")
    @Expose
    private double temp;

    @SerializedName("pressure")
    @Expose
    private double pressure;

    @SerializedName("temp_min")
    @Expose
    private double temp_min;

    @SerializedName("temp_max")
    @Expose
    private double temp_max;

    public String getTemp() {
        return Double.toString(temp);
    }

    public String getPressure() {
        return Double.toString(pressure);
    }

    public String getTempMin() {
        return Double.toString(temp_min);
    }

    public String getTempMax() {
        return Double.toString(temp_max);
    }
}
